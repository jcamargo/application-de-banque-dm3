<?php
  require_once('User.php');
  require_once('include.php');
  session_start();
  
  $url_redirect = "index.php";
  
  if (isset($_POST['action'])) {
  
      if ($_POST['action'] == 'authenticate') {
          

          if (!isset($_POST['login']) || !isset($_POST['password']) || $_POST['login'] == "" || $_POST['password'] == "" ||  $_POST['g-recaptcha-response'] == "") {
              
              $url_redirect = "index.php?nullvalue";
              
          } else {
            $secret = '6Ld4-ucaAAAAAElA4LlvyXukxcIlQLBm0tGDhJry';
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if($responseData->success){
              $login=$_POST['login'];
              $password=$_POST['password'];
                $utilisateur = findUserByLoginPwd($login, $password);
                
                if ($utilisateur == false) {
                  $url_redirect = "index.php?badvalue";
                  
                } else {
                  // authentification réussie
                  $_SESSION["connected_user"] = $utilisateur;
                  $_SESSION["listeUsers"] = findAllUsers();
                  $url_redirect = "accueil.php";
                }
            }else{
              $url_redirect = "index.php?isRobot";
            }

          }
          
      } else if ($_POST['action'] == 'disconnect') {
          /* ======== DISCONNECT ======== */
          unset($_SESSION["connected_user"]);
          $url_redirect = $_POST['loginPage'] ;
          
      } else if ($_POST['action'] == 'transfert') {
          /* ======== TRANSFERT ======== */
          if (is_numeric ($_POST['montant'])) {
              transfert($_POST['destination'],$_SESSION["connected_user"]["numero_compte"], $_POST['montant']);
              $_SESSION["connected_user"]["solde_compte"] = $_SESSION["connected_user"]["solde_compte"] -  $_POST['montant'];
              $url_redirect = "accueil.php";
              
          } else {
              $url_redirect = "User.php?bad_montant=".$_POST['montant'];
          }
       
      } else if ($_POST['action'] == 'sendmsg') {
          addMessage($_POST['to'],$_SESSION["connected_user"]["id_user"],$_POST['sujet'],$_POST['corps']);
          $url_redirect = "accueil.php?msg_ok";
              
      } else if ($_POST['action'] == 'register'){
        $nom=$_POST['nom'];
        $prenom=$_POST['prenom'];
        $login=$_POST['login'];
        $pwd=$_POST['password'];
        $profil=$_POST['role'];
        $numeroCompte=$_POST['nombreCompte'];
        $solde=$_POST['solde'];
        $hash = password_hash($pwd, PASSWORD_DEFAULT);
        addNewUser ($login, $hash,$profil,$nom,$prenom,$numeroCompte,$solde);

        if($_POST['typeRegistre']=="Intern"){

          $url_redirect = "accueil.php";
        }else{
          $url_redirect = "index.php";

        }
      }

       
  }  
  
  header("Location: $url_redirect");

?>
