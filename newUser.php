<html>
<?php session_start();
require_once('include.php');?>
<head>
<title> Creer nouvel compte </title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">

			<li class="nav-item"><a class="nav-link" href="messagerie.php"> Messagerie</a></li>
			<li class="nav-item"><a class="nav-link" href="virement.php"> Effectuer un virement</a></li>
			
      <?php 
    $utilisateur = $_SESSION["connected_user"];
    if($utilisateur["profil_user"] =="EMPLOYE"){
        echo "<li class='nav-item'><a class='nav-link' href='ficheClients.php'> Fiche client </a></li>";
        echo "<li class='nav-item'><a class='nav-link' href='newUser.php'> Creer nouvel compte </a></li>";

    }
?>

		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
            <form method="POST" action="Control.php">
            <input type="hidden" name="action" value="disconnect">
            <input type="hidden" name="loginPage" value="Control.php?disconnect">
            <button class="btn btn-danger"  >Déconnexion</button>
                </form>
				</li>
			</ul>
		</div>
	</nav>


<div class="container-fluid pt-4">
    <div class="row justify-content-center align-items-center h-100">
        <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3">
<form method="POST" action="Control.php">
<input type="hidden" name="action" value="register">
<div class="form-group">
  <label>Nom</label> 
  <input type="text" class="form-control" name="nom"> 
  </div>
  <div class="form-group">
  <label>Prenom</label> 
  <input type="text" class="form-control" name="prenom"> 
  </div>
  <div class="form-group">
  <label>Login</label> 
  <input type="text" class="form-control" name="login"> 
  </div>
  <div class="form-group">
   <label>Password</label>
    <input type="password" class="form-control" name="password" >
    
  </div>
    <label> Role </label><br>
					<div class="form-check-inline">

						<label class="form-check-label"><input type="radio"
							class="form-check-input" name="role" id="employe" value="EMPLOYE"
							checked> Employe</label>
					</div>
					<div class="form-check-inline">
						<label class="form-check-label"><input type="radio"
							class="form-check-input" name="role" id="client" value="CLIENT">
							Client</label>
					</div>



  <div class="form-group">
  <label>Numero de compte</label> 
  <input type="text" class="form-control" name="nombreCompte"> 
  </div>

  <div class="form-group" >
     <label> Solde </label>
    <input type="int" size="20" class="form-control" name="solde"> 
    </div>
    <input type="hidden"  name="typeRegistre" value="Intern"> 
	<input type="submit" value="Sign up" class="btn btn-primary">

</form>

        </div>
    </div>
</div>

</body>
</html>