<?php
require_once('include.php');
function getMySqliConnection() {
  $file = parse_ini_file("./config/config.ini");
        $host = $file["host"];
        $port = $file["port"];
        $db   = $file["db"];
        $user = $file["user"];
        $pass = $file["pass"];
  return new mysqli($host, $user,$pass, $db);
}

function findUserByLoginPwd($login, $pwd) {
  $mysqli = getMySqliConnection();
  echo "erro en la connecion";
  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
      $utilisateur = false;
  }
  else {
    // Pour faire vraiment propre, on devrait tester si le prepare et le execute se passent bien
    //$hash = password_hash($pwd, PASSWORD_DEFAULT);
    $stmt = $mysqli->prepare("select nom,prenom,mot_de_passe,login,id_user,numero_compte,profil_user,solde_compte from users where login=?");  
    $stmt->bind_param("s", $login); // on lie les paramètres de la requête préparée avec les variables
    $stmt->execute();
    $stmt->bind_result($nom,$prenom,$password,$username,$id_user,$numero_compte,$profil_user,$solde_compte); // on prépare les variables qui recevront le résultat
    if ($stmt->fetch()) {
        // les identifiants sont corrects => on renvoie les infos de l'utilisateur
        if(password_verify($pwd, $password)){
          $utilisateur = array ("nom" => $nom,
          "prenom" => $prenom,
          "login" => $username,
          "id_user" => $id_user,
          "numero_compte" => $numero_compte,
          "profil_user" => $profil_user,
          "solde_compte" => $solde_compte);
        } else{
          $utilisateur = false;
        }

    } else {
        // les identifiants sont incorrects
        $utilisateur = false;
    }
    $stmt->close();
    
    $mysqli->close();
}

return $utilisateur;
}


function findAllUsers() {
  $mysqli = getMySqliConnection();
  $listeUsers = array();
  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
        $req="select nom,prenom,login,id_user from users";
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      } else {
          while ($unUser = $result->fetch_assoc()) {
            $listeUsers[$unUser['id_user']] = $unUser;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeUsers;
}




function findReceiversByProfil() {
  $mysqli = getMySqliConnection();
  $listeUsers = array();
  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      if($_SESSION["connected_user"]["profil_user"] =="CLIENT"){
        $req="select nom,prenom,login,id_user from users";
        
      }
      else {
        $req="select nom,prenom,login,id_user from users where profil_user='CLIENT'";
      }
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      } else {
          while ($unUser = $result->fetch_assoc()) {
            $listeUsers[$unUser['id_user']] = $unUser;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeUsers;
}

function findAllUserAllInform() {
  $mysqli = getMySqliConnection();
  $listeUsers = array();
  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      if($_SESSION["connected_user"]["profil_user"] =="CLIENT"){
        $req="select nom,prenom,login,id_user,solde_compte,numero_compte from users";
        
      }
      else {
        $req="select nom,prenom,login,id_user,solde_compte,numero_compte from users where profil_user='CLIENT'";
      }
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      } else {
          while ($unUser = $result->fetch_assoc()) {
            $listeUsers[$unUser['id_user']] = $unUser;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeUsers;
}


function transfert($destination, $src, $montant) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
      $utilisateur = false;
  } else {
      $req="update users set solde_compte=solde_compte+$montant where id_user='$destination'";
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      }
      $req="update users set solde_compte=solde_compte-$montant where numero_compte='$src'";
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      }
      $mysqli->close();
  }

  return $utilisateur;
}


function findMessagesInbox($userid) {
  $mysqli = getMySqliConnection();

  $listeMessages = array();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      $req="select id_msg,sujet_msg,corps_msg,u.nom,u.prenom from messages m, users u where m.id_user_from=u.id_user and id_user_to=".$userid;
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      } else {
          while ($unMessage = $result->fetch_assoc()) {
            $listeMessages[$unMessage['id_msg']] = $unMessage;
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $listeMessages;
}


function addMessage($to,$from,$subject,$body) {
  $mysqli = getMySqliConnection();

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
      $req="insert into messages(id_user_to,id_user_from,sujet_msg,corps_msg) values($to,$from,'$subject','$body')";
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      }
      $mysqli->close();
  }

}

function addNewUser ($login, $pwd,$profil,$nom,$prenom,$numeroCompte,$solde){
  $mysqli = getMySqliConnection();
  $stmt = $mysqli->prepare("insert into users (`login`, `mot_de_passe`, `profil_user`, `nom`, `prenom`, `numero_compte`, `solde_compte`) values (?,?,?,?,?,?,?)");
    $stmt->bind_param("sssssii", $login, $pwd,$profil,$nom,$prenom,$numeroCompte,$solde); 
    $stmt->execute();
}

?>
