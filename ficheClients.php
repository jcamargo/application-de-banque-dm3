<html>
<?php 
  session_start();
  require_once('User.php');
  require_once('include.php');

?>
<head>
<title> Gestion de clients </title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">

			<li class="nav-item"><a class="nav-link" href="messagerie.php"> Messagerie</a></li>
			<li class="nav-item"><a class="nav-link" href="virement.php"> Effectuer un virement</a></li>
			
      <?php 
    $utilisateur = $_SESSION["connected_user"];
    if($utilisateur["profil_user"] =="EMPLOYE"){
        echo "<li class='nav-item'><a class='nav-link' href='ficheClients.php'> Fiche client </a></li>";
        echo "<li class='nav-item'><a class='nav-link' href='newUser.php'> Creer nouvel compte </a></li>";

    }
?>



		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
            <form method="POST" action="Control.php">
            <input type="hidden" name="action" value="disconnect">
            <input type="hidden" name="loginPage" value="Control.php?disconnect">
            <button class="btn btn-danger"  >Déconnexion</button>
                </form>
				</li>
			</ul>
		</div>
	</nav>
  <div class="card">
		<div class="card-header">
    <h2> Clients disponibles </h2>
      
		</div>
		<div class="card-body">
 <!-- obtenir tous les users dont je suis le destinaire -->
 <table class="table table-hover">
              <tr>
                <th scope="col" ># compte </th>
                <th scope="col" >Nom</th>
                <th scope="col" >Prenom</th>
                <th scope="col" >Email</th>
                <th scope="col" >Solde</th>
              </tr>
              <?php
              $users = findAllUserAllInform();

              foreach ($users as $cle => $user) {
                echo '<tr>';
                echo '<td scope="row">'.$user['numero_compte'].'</td>';
                echo '<td>'.$user['nom'].'</td>';
                echo '<td>'.$user['prenom'].'</td>';
                echo '<td>'.$user['login'].'</td>';
                echo '<td>'.$user['solde_compte'].'</td>';
                echo '<td><a href="virement.php"> Faire virement </a></td>';

                echo '</tr>';
              }
               ?>
            </table>



		</div>
	</div>

</body>
</html>