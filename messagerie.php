<html>
<?php 
  session_start();
  require_once('User.php');
  require_once('include.php');

?>

<head>
<title> Messagerie </title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">

			<li class="nav-item"><a class="nav-link" href="messagerie.php"> Messagerie</a></li>
			<li class="nav-item"><a class="nav-link" href="virement.php"> Effectuer un virement</a></li>
			
      
      <?php 
    $utilisateur = $_SESSION["connected_user"];
    if($utilisateur["profil_user"] =="EMPLOYE"){
        echo "<li class='nav-item'><a class='nav-link' href='ficheClients.php'> Fiche client </a></li>";
        echo "<li class='nav-item'><a class='nav-link' href='newUser.php'> Creer nouvel compte </a></li>";

    }
?>
		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
            <form method="POST" action="Control.php">
            <input type="hidden" name="action" value="disconnect">
            <input type="hidden" name="loginPage" value="Control.php?disconnect">
            <button class="btn btn-danger"  >Déconnexion</button>
                </form>
				</li>
			</ul>
		</div>
	</nav>

  <div class="card">
		<div class="card-header">
        <h2> Mes messages </h2>

		</div>
		<div class="card-body">
    <div>
            <table class="table table-hover">
              <tr>
              <th scope="col" >Expéditeur</th>
              <th scope="col" >Sujet</th>
              <th scope="col" >Message</th>
              </tr>
              <?php
              $myMessages = findMessagesInbox($_SESSION['connected_user']["id_user"]);
              foreach ($myMessages as $cle => $message) {
                echo '<tr>';
                echo '<td scope="row">'.$message['nom'].' '.$message['prenom'].'</td>';
                echo '<td>'.htmlentities($message['sujet_msg']).'</td>';
                echo '<td>'.htmlentities($message['corps_msg']).'</td>';
                echo '</tr>'; 
              }
               ?>
            </table>
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
              Nouvel message</button>

<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"> Envoyer un message </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <form method="POST" action="Control.php">
          <input type="hidden" name="action" value="sendmsg">
              <div class="form-group" >
                  <label>Destinataire : </label>
                  <select name="to" class="custom-select" >
                    <?php
                    $users = findReceiversByProfil();
                    foreach ($users as $id => $user) {
                      echo '<option value="'.$user['id_user'].'">'.$user['nom'].' '.$user['prenom'].'</option>';
                    }
                    ?>
                  </select>
              </div>
              <div class="form-group" >
                  <label>Sujet : </label>
                  <input type="text" size="20" name="sujet">
              </div>
              <div  class="form-group" >
                  <label>Message : </label>
                  <textarea name="corps" cols="25" rows="3""></textarea>
              </div>
              <button class="btn btn-primary" >Envoyer</button>
             
        </form>

      </div>

    </div>
  </div>
</div>


		</div>
	</div>

</body>
</html>