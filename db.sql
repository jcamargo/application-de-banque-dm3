CREATE TABLE users ( id_user INTEGER(11) NOT NULL AUTO_INCREMENT ,
         login VARCHAR(500) NOT NULL,
         mot_de_passe VARCHAR(500) NOT NULL,
         profil_user VARCHAR (20) CHECK ( profil_user IN ('CLIENT', 'EMPLOYE')) ,
         nom VARCHAR(500) NOT NULL,
         prenom VARCHAR(500)NOT NULL, 
         numero_compte INTEGER(20) NOT NULL, 
         solde_compte INTEGER(20) NOT NULL, 
         CONSTRAINT user_pk PRIMARY KEY (id_user) )

CREATE TABLE messages (
        id_msg INTEGER (100) NOT NULL AUTO_INCREMENT,
        id_user_to INTEGER(11) NOT NULL,
        id_user_from INTEGER(11) NOT NULL,
        sujet_msg VARCHAR(200) NOT NULL,
        corps_msg  VARCHAR (500) NOT NULL,
        CONSTRAINT messagesPk PRIMARY KEY (id_msg,id_user_to,id_user_from),
        FOREIGN KEY (id_user_to) REFERENCES users(id_user),
        FOREIGN KEY (id_user_from) REFERENCES users(id_user)
);

 
INSERT INTO `users` (`login`, `mot_de_passe`, `profil_user`, `nom`, `prenom`, `numero_compte`, `solde_compte`) VALUES
('root@root', '$2y$10$y6eVAsByRdPyBlEJplR7mOwaQi8bTADKreMYkjfRJkkfszepBarIW', 'EMPLOYE', 'Root', 'Root', 1234, 10000);





