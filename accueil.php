<html>
<?php session_start();
require_once('include.php');?>
<head>
<title>Accueil</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand -->
		<a class="navbar-brand" href="#">UTC</a>

		<!-- Links -->
		<ul class="navbar-nav">

			<li class="nav-item"><a class="nav-link" href="messagerie.php"> Messagerie</a></li>
			<li class="nav-item"><a class="nav-link" href="virement.php"> Effectuer un virement</a></li>
			
            <?php 
    $utilisateur = $_SESSION["connected_user"];
    if($utilisateur["profil_user"] =="EMPLOYE"){
        echo "<li class='nav-item'><a class='nav-link' href='ficheClients.php'> Fiche client </a></li>";
        echo "<li class='nav-item'><a class='nav-link' href='newUser.php'> Creer nouvel compte </a></li>";
    
    }
?>



		</ul>
		<div class="navbar-collapse collapse">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
            <form method="POST" action="Control.php">
            <input type="hidden" name="action" value="disconnect">
            <input type="hidden" name="loginPage" value="Control.php?disconnect">
            <button class="btn btn-danger"  >Déconnexion</button>
                </form>
				</li>
			</ul>
		</div>
	</nav>


    <div class="card">
		<div class="card-header">
        <h2><?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?> - Mon compte</h2>

		</div>
		<div class="card-body">
			<h5 class="card-title"> Vos informations personnelles</h5>

            <div class="fieldset">
              <div class="field">
                  <label>Login : </label><span><?php echo $_SESSION["connected_user"]["login"];?></span>
              </div>
              <div class="field">
                  <label>N° compte : </label><span><?php echo $_SESSION["connected_user"]["numero_compte"];?></span>
              </div>
              <div class="field">
                  <label>Profil : </label><span><?php echo $_SESSION["connected_user"]["profil_user"];?></span>
              </div>
              <div class="field">
                  <label>Solde : </label><span><?php echo $_SESSION["connected_user"]["solde_compte"];?></span>
              </div>
          </div>


		</div>
	</div>

</body>
</html>